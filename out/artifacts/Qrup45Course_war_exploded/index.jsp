<%--
  Created by IntelliJ IDEA.
  User: HaMuSHaN
  Date: 15.08.2020
  Time: 07:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Qrup45</title>
    <meta name="keywords" content="Java,C#,Web" />
    <meta name="author" content="Fuad Pashabeyli" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <body>
  <h1>Salam Qrup45 </h1>
  <hr>
  <b><i>Salam</i></b> <br>
  <strong>Salam</strong> <br>
  <del>salam</del> <br>
  x<sub>2</sub>,y<sup>2</sup>
  <hr>
  <a href="http://orient-itm.com">Orient- e daxil ol!</a> <br>
  <a href="http://google.com" target="_blank">Google-a daxil ol!</a>
  <hr>
  <img src="images/baku.jpg" alt="Baku" height="400" width="400" border="1"/> <br>
  <table border="1" id="studentDataTblId">
    <caption>Student Data</caption>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Surname</th>
      <th>Address</th>
      <th>Date of birth</th>
      <th>Phone</th>
    </tr>
    <tr>
      <td>1</td>
      <td>Eli</td>
      <td>Eliyev</td>
      <td>Baki</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
    <tr>
      <td>2</td>
      <td>Veli</td>
      <td>Veliyev</td>
      <td>Sumqayit</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
    <tr>
      <td>3</td>
      <td>Qasim</td>
      <td>Qasimli</td>
      <td>Gence</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
  </table>
  <table border="1" id="teacherDataTblId">
    <caption>Teacher DAta</caption>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Surname</th>
      <th>Address</th>
      <th>Date of birth</th>
      <th>Phone</th>
    </tr>
    <tr>
      <td>1</td>
      <td>Mahir</td>
      <td>Eliyev</td>
      <td>Baki</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
    <tr>
      <td>2</td>
      <td>Famil</td>
      <td>Veliyev</td>
      <td>Sumqayit</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
    <tr>
      <td>3</td>
      <td>Tahir</td>
      <td>Qasimli</td>
      <td>Gence</td>
      <td>Date of birth</td>
      <td><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></td>
    </tr>
  </table>
  <hr>
  <ol>
    <li>3</li>
    <li>Qasim</li>
    <li>Qasimli</li>
    <li>Gence</li>
    <li>Date of birth</li>
    <li><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></li>
  </ol>
  <br>
  <ul>
    <li>3</li>
    <li>Qasim</li>
    <li>Qasimli</li>
    <li>Gence</li>
    <li><a href="https://www.google.com">Google</a> </li>
    <li><img src="images/baku.jpg" alt="Baku" height="100" width="100" border="1"/></li>
  </ul>
  </body>
</html>
