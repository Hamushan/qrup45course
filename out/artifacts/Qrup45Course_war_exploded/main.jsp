<%--
  Created by IntelliJ IDEA.
  User: HaMuSHaN
  Date: 15.08.2020
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Course</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
<div id = "container" >
    <div id = "header" >
        <h1 style="margin-bottom: 0;">Course</h1>
    </div>
    <div id = "menu"  >
        <input type="button" value="Student Data" id = "studentDataBtnId" class="btnDesign" onclick="alert('Hello Qrup45')"/> <br>
        <input type="button" value="Teacher Data" id = "teacherDataBtnId" class="btnDesign"/> <br>
        <input type="button" value="Lesson Data" id = "lessonDataBtnId" class="btnDesign"/> <br>
        <input type="button" value="Payment Data" id = "paymentDataBtnId" class="btnDesign"/> <br>
    </div>
    <div id = "content">
        <table border="1" id="studentDataTblId">
            <caption>Student Data</caption>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Address</th>
                <th>Date of birth</th>
                <th>Phone</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Eli</td>
                <td>Eliyev</td>
                <td>Baki</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Veli</td>
                <td>Veliyev</td>
                <td>Sumqayit</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Qasim</td>
                <td>Qasimli</td>
                <td>Gence</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
        </table>
        <br>
        <table border="1" id="teacherDataTblId">
            <caption>Teacher DAta</caption>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Address</th>
                <th>Date of birth</th>
                <th>Phone</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Mahir</td>
                <td>Eliyev</td>
                <td>Baki</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Famil</td>
                <td>Veliyev</td>
                <td>Sumqayit</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Tahir</td>
                <td>Qasimli</td>
                <td>Gence</td>
                <td>Date of birth</td>
                <td><img src="images/baku.jpg" alt="Baku" height="70" width="70" border="1"/></td>
            </tr>
        </table>
    </div>
    <div id="footer" >
        Copyright
    </div>
</div>
</body>
</html>
